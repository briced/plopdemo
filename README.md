The project has been tested with node 4.4.5 (LTS)

How to use:

* If you use nvm, do 'nvm use'
* Then run 'npm install'
* Then run 'npm run plop'

To clear, use 'npm run clear (will delete app/ & tests/)'
