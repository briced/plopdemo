'use strict';

angular.module('services.{{lowerCase package}}', [

])
    .factory('{{firstLetterUpperCase name}}Service', () => {

        class {{firstLetterUpperCase name}}Service {
            constructor() {

            }
        }

        return new {{firstLetterUpperCase name}}Service();
    });
