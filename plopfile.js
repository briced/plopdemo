'use strict';
const lodash = require('lodash');

const isEmpty = lodash.isEmpty;

const isNotEmptyFor = (name) => {
    return (value) => {
        if (isEmpty(value)) {
            return name + ' is required';
        }
        return true;
    };
};

module.exports = function (plop) {
    plop.addHelper('firstLetterUpperCase', (txt) => {
        return txt.substring(0,1).toUpperCase() + txt.substring(1).toLowerCase();
    });

    // generators
    plop.setGenerator('controller', {
        description: 'Add a controller',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'What is your controller name? (without \'Controller\' extension)',
                validate: isNotEmptyFor('name')
            },
            {
                type: 'input',
                name: 'package',
                message: 'What is your controller package?',
                validate: isNotEmptyFor('package')
            },
            {
                type: 'input',
                name: 'path',
                message: 'What is your controller path?',
                validate: isNotEmptyFor('path')
            }
        ],
        actions: [
            (answers) => {
                console.log(`You choose: \n
                - name: ${answers.name}
                - package: ${answers.package}
                - path: ${answers.path}`);
                return '';
            },
            {
                type: 'add',
                path: 'app/modules/{{camelCase path}}/{{camelCase name}}Controller.js',
                templateFile: 'plop-templates/modules/controller.js'
            },
            {
                type: 'add',
                path: 'app/modules/{{camelCase path}}/{{camelCase name}}.html',
                templateFile: 'plop-templates/modules/view.html'
            },
            {
                type: 'add',
                path: 'tests/unit/modules/{{camelCase path}}/{{camelCase name}}.tests.js',
                templateFile: 'plop-templates/modules/test.js'
            }
        ]
    });

    plop.setGenerator('service', {
        description: 'Add a service',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'What is your service name? (without \'Service\' extension)',
                validate: isNotEmptyFor('name')
            },
            {
                type: 'input',
                name: 'package',
                message: 'What is your service package?',
                validate: isNotEmptyFor('package')
            }
        ],
        actions: [
            (answers) => {
                console.log(`You choose: \n
                - name: ${answers.name}
                - package: ${answers.package}`);
                return '';
            },
            {
                type: 'add',
                path: 'app/services/{{camelCase name}}Service.js',
                templateFile: 'plop-templates/services/service.js'
            },
            {
                type: 'add',
                path: 'tests/unit/services/{{camelCase name}}Service.js',
                templateFile: 'plop-templates/services/test.js'
            }
        ]
    });

    plop.setGenerator('filter', {
        description: 'Add a filter',
        prompts: [
            {
                type: 'input',
                name: 'name',
                message: 'What is your service name? (without \'Filter\' extension)',
                validate: isNotEmptyFor('name')
            }
        ],
        actions: [
            (answers) => {
                console.log(`You choose: \n
                - name: ${answers.name}`);
                return '';
            },
            {
                type: 'add',
                path: 'app/filters/{{camelCase name}}Filter.js',
                templateFile: 'plop-templates/filters/filter.js'
            },
            {
                type: 'add',
                path: 'tests/unit/filters/{{camelCase name}}Filter.js',
                templateFile: 'plop-templates/filters/test.js'
            }
        ]
    });
};
